//
//  Network+Cities.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

extension Network: CitiesNetworkProtocol {
    
    func getCountries(complete: @escaping ((_ response:[Country]?, _ error:NetworkError?)->())) {
        manager.request(urlApiString(path: "countries"), method: .get, parameters: nil).responseObject { (response: DataResponse<NetworkResponse<Country>>) in
            if let countries = response.result.value?.result {
                complete(countries, nil)
            } else {
                complete(nil, response.result.value?.error)
            }
        }
    }
}
