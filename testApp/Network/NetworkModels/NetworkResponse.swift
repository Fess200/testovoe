//
//  NetworkResponse.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper

class NetworkResponse<T:Mappable>: Mappable {
    
    var timestamp:String?
    var error:NetworkError?
    var result:[T]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        timestamp <- map["Timestamp"]
        error <- map["Error"]
        result <- map["Result"]
    }
}
