//
//  NetworkError.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper

class NetworkError: Mappable {

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
    }
}
