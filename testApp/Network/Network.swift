//
//  Network.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Alamofire

class Network: NSObject {
    
    let manager: SessionManager
    
    override init() {
        
        manager = Alamofire.SessionManager()
        
        super.init()
    }
    
    func urlApiString(path:String) -> String {
        return urlString(path: "api/\(path)")
    }
    
    func urlString(path:String) -> String {
        return "https://atw-backend.azurewebsites.net/\(path)"
    }
    
}
