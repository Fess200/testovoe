//
//  DIProtocols+Router.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol RouterProtocol {
    var rootController: UIViewController {get}
}
