//
//  ManagerAssembly.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Swinject

let assembler = try! Assembler(assemblies: [
    ManagerAssembly()
    ])

class ManagerAssembly: Assembly {
    func assemble(container: Container) {
        
        let network = Network()
        
        container.register(CitiesServiceProtocol.self) { _ in CitiesService(network: network) }.inObjectScope(.container)
        
        container.register(RouterProtocol.self) { _ in Router() }
    }
}
