//
//  DIProtocols+Network.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Foundation

protocol CitiesNetworkProtocol {
    func getCountries(complete: @escaping ((_ response:[Country]?, _ error:NetworkError?)->()))
}
