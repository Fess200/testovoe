//
//  DIProtocols+Service.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol CitiesServiceProtocol {
    var selectedCity:Int? {get set}
    
    func getCountries(complete: @escaping ((_ response:[Country]?, _ error:Error?)->()))
    
    func getFavouritesCities() -> [City]
    func isFavourite(city:City) -> Bool
    func addFavourite(city:City)
    func removeFavourite(city:City)
}
