//
//  City.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper
import RealmSwift

class City: Object, Mappable {
    
    private static let keyId = "Id"
    
    dynamic var id = 0
    dynamic var name:String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        guard let jsonId = map.JSON[City.keyId] as? Int else {
            return nil
        }
        
        self.init()
        
        id = jsonId
    }
    
    func mapping(map: Map) {
        name <- map["Name"]
    }
    
}
