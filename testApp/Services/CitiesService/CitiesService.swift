//
//  CitiesService.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class CitiesService: NSObject, CitiesServiceProtocol {
    
    private let network:CitiesNetworkProtocol
    private let countriesStore:CountriesStore
    
    init(network:CitiesNetworkProtocol) {
        self.network = network
        self.countriesStore = CountriesStore()
        super.init()
    }
    
    var selectedCity:Int? {
        get {
            return countriesStore.selectedCity
        }
        set {
            countriesStore.selectedCity = newValue
        }
    }
    
    func getCountries(complete: @escaping ((_ response:[Country]?, _ error:Error?)->())) {
        
        if let countries = self.countriesStore.countries {
            complete(countries, nil)
            return
        }
        
        network.getCountries { (countries:[Country]?, error:NetworkError?) in
            self.countriesStore.countries = countries
            // generate error if needed
            complete(countries, nil)
        }
    }
    
    func getFavouritesCities() -> [City] {
        return countriesStore.getFavouritesCities()
    }
    
    func isFavourite(city:City) -> Bool {
        let cities = countriesStore.getFavouritesCities()
        
        for cityStore in cities {
            if cityStore.id == city.id {
                return true
            }
        }
        
        return false
    }
    
    func addFavourite(city:City) {
        countriesStore.addFavourite(city: city)
    }
    
    func removeFavourite(city:City) {
        countriesStore.removeFavourite(city: city)
    }
}
