//
//  CountriesStore.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RealmSwift

class CountriesStore: NSObject {
    
    private let keySelectedCity = "SelectedCity"
    
    var selectedCity:Int? {
        get {
            return UserDefaults.standard.object(forKey: keySelectedCity) as! Int?
        }
        set {
            if let selectedCity = newValue {
                UserDefaults.standard.set(selectedCity, forKey: keySelectedCity)
            } else {
                UserDefaults.standard.removeObject(forKey: keySelectedCity)
            }
            UserDefaults.standard.synchronize()
        }
    }
    
    var countries:[Country]?
    
    let realm: Realm
    
    override init() {
        realm = try! Realm()
    }
    
    func getFavouritesCities() -> [City] {
        return realm.objects(City.self).map{$0}
    }
    
    func addFavourite(city:City) {
        try! realm.write {
            realm.create(City.self, value: city, update: false)
        }
    }
    
    func removeFavourite(city:City) {
        
        let cities = realm.objects(City.self).filter("id = \(city.id)")
        
        try! realm.write {
            realm.delete(cities)
        }
    }
    
}
