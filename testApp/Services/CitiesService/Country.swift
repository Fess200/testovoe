//
//  Country.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper

class Country: Mappable {
    
    private static let keyId = "Id"
    
    var id:Int!
    var name:String?
    var imageLink:String?
    var cities:[City]?
    
    required init?(map: Map) {
        guard let _ = map.JSON[Country.keyId] else {
            return nil
        }
    }
    
    func mapping(map: Map) {
        id <- map[Country.keyId]
        name <- map["Name"]
        imageLink <- map["ImageLink"]
        cities <- map["Cities"]
    }
    
}
