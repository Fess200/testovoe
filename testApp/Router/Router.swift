//
//  Router.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class Router: NSObject, RouterProtocol {
    
    let storyBoard: UIStoryboard
    var tabBarController: UITabBarController!
    
    override init() {
        
        storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        super.init()
    }
    
    var rootController: UIViewController {
        
        tabBarController = UITabBarController.init()
        
        let controllers = [prepareCities(), prepareFavourites(), prepareSettings()]
        tabBarController.viewControllers = controllers
        
        return tabBarController
    }
    
    private func prepareCities() -> UITableViewController {
        let controller = storyBoard.instantiateViewController(withIdentifier: "VCCities") as! VCCities
        controller.presenter = CitiesPresenter(view: controller)
        controller.tabBarItem.title = "Страны"
        return controller
    }
    
    private func prepareFavourites() -> UITableViewController {
        let controller = storyBoard.instantiateViewController(withIdentifier: "VCFavourites") as! VCFavourites
        controller.presenter = FavouritesPresenter(view: controller)
        controller.tabBarItem.title = "Избранное"
        return controller
    }
    
    private func prepareSettings() -> UIViewController {
        let controller = storyBoard.instantiateViewController(withIdentifier: "VCSettings") as! VCSettings
        controller.tabBarItem.title = "Настройки"
        return controller
    }
    
}
