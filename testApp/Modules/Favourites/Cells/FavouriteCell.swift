//
//  FavouriteCell.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol FavouriteCellDelegate: NSObjectProtocol {
    func removeFavourite(city:City)
}

class FavouriteCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    
    weak var delegate:FavouriteCellDelegate?
    
    var city: City! {
        didSet {
            labelName.text = city?.name
        }
    }

    @IBAction func onDelete(_ sender: Any) {
        delegate?.removeFavourite(city: city)
    }

}
