//
//  VCFavourites.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCFavourites: UITableViewController, FavouritesPresenterDelegate, FavouriteCellDelegate {

    private let identifierFavouriteCell = "FavouriteCell"
    
    var presenter: FavouritesPresenter!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.requestData()
    }
    
    // MARK: - FavouritesPresenterDelegate
    
    func reloadData() {
        tableView.reloadData()
    }
    
    // MARK: - FavouriteCellDelegate
    
    func removeFavourite(city:City) {
        presenter.removeFavourite(city: city)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getCountFavouriteCities()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierFavouriteCell, for: indexPath) as! FavouriteCell
        cell.city = presenter.getFavouriteCity(index: indexPath.row)
        cell.delegate = self
        return cell
    }

}
