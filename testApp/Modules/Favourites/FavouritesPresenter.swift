//
//  FavouritesPresenter.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol FavouritesPresenterDelegate {
    func reloadData()
}

class FavouritesPresenter: NSObject {
    
    private let view:FavouritesPresenterDelegate
    private let service:CitiesServiceProtocol
    
    private var cities:[City]?
    
    init(view:FavouritesPresenterDelegate) {
        self.view = view
        self.service = assembler.resolver.resolve(CitiesServiceProtocol.self)!
    }
    
    func getCountFavouriteCities() -> Int {
        
        guard let cities = cities else {
            return 0
        }
        
        return cities.count
    }
    
    func getFavouriteCity(index:Int) -> City {
        return cities![index]
    }
    
    func requestData() {
        cities = self.service.getFavouritesCities()
        self.view.reloadData()
    }
    
    func removeFavourite(city:City) {
        service.removeFavourite(city: city)
        requestData()
    }
    
}
