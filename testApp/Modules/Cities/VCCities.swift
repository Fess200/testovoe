//
//  VCCities.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCCities: UITableViewController, CitiesPresenterDelegate, CountryHeaderDelegate, CityCellDelegate {

    private let identifierCityCell = "CityCell"
    
    var presenter: CitiesPresenter!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.requestData()
    }
    
    // MARK: - CitiesPresenterDelegate
    
    func reloadData() {
        tableView.reloadData()
        
        if let indexPath = presenter.selectedIndex {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    // MARK: - CountryHeaderDelegate
    
    func tapHeaderCountry(country:Country) {
        presenter.changeStateOpenCountry(country: country)
    }
    
    // MARK: - CityCellDelegate
    
    func addFavourite(city:City) {
        presenter.addFavourite(city: city)
    }
    
    func removeFavourite(city:City) {
        presenter.removeFavourite(city: city)
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let nib = Bundle.main.loadNibNamed("CountryHeader", owner: self, options: nil) {
            if let view = nib.first as? CountryHeader {
                view.delegate = self
                view.country = presenter.getCountry(index: section)
                return view
            }
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.selectedIndex = indexPath
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.getCountCountries()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getCountCitiesCountry(index: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierCityCell, for: indexPath) as! CityCell
        cell.city = presenter.getCity(indexCountry: indexPath.section, indexCity: indexPath.row)
        cell.isFavourite = presenter.isFavourite(city: cell.city)
        cell.delegate = self
        return cell
    }

}
