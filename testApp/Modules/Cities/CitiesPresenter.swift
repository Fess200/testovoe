//
//  CitiesPresenter.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol CitiesPresenterDelegate {
    func reloadData()
}

class CitiesPresenter: NSObject {
    
    private let view:CitiesPresenterDelegate
    private var service:CitiesServiceProtocol
    
    private var countries:[Country]?
    
    private var openIndexCountries = [Int]()
    
    var selectedIndex:IndexPath? {
        get {
            guard let selectedCity = service.selectedCity, let countries = countries else {
                return nil
            }
            
            for section in 0..<countries.count {
                let country = countries[section]
                if let cities = country.cities {
                    for row in 0..<cities.count {
                        let city = cities[row]
                        if city.id == selectedCity {
                            return IndexPath(row: row, section: section)
                        }
                    }
                }
            }
            
            return nil
        }
        set {
            if let selectedIndex = newValue {
                service.selectedCity = getCity(indexCountry: selectedIndex.section, indexCity: selectedIndex.row).id
            } else {
                service.selectedCity = nil
            }
        }
    }
    
    init(view:CitiesPresenterDelegate) {
        self.view = view
        self.service = assembler.resolver.resolve(CitiesServiceProtocol.self)!
    }
    
    func requestData() {
        self.service.getCountries { (countries:[Country]?, error:Error?) in
            self.countries = countries
            self.view.reloadData()
        }
    }
    
    func getCountCountries() -> Int {
        
        guard let countries = countries else {
            return 0
        }
        
        return countries.count
    }
    
    func getCountCitiesCountry(index:Int) -> Int {
        
        if isOpenCountry(index: index) {
            guard let cities = countries![index].cities else {
                return 0
            }
            
            return cities.count
        } else {
            return 0
        }
    }
    
    func getCountry(index:Int) -> Country {
        return countries![index]
    }
    
    func getCity(indexCountry:Int, indexCity:Int) -> City {
        return countries![indexCountry].cities![indexCity]
    }
    
    func isFavourite(city:City) -> Bool {
        return service.isFavourite(city: city)
    }
    
    func addFavourite(city:City) {
        service.addFavourite(city: city)
        self.view.reloadData()
    }
    
    func removeFavourite(city:City) {
        service.removeFavourite(city: city)
        self.view.reloadData()
    }
    
    private func getIndexCountry(country:Country) -> Int? {
        
        for index in 0..<countries!.count {
            let countryData = countries![index]
            if countryData.id == country.id {
                return index
            }
        }
        
        return nil
    }
    
    func changeStateOpenCountry(country:Country) {
        
        if let index = getIndexCountry(country: country) {
            
            if isOpenCountry(index: index) {
                
                for i in 0..<openIndexCountries.count {
                    let openIndex = openIndexCountries[i]
                    if openIndex == index {
                        openIndexCountries.remove(at: i)
                        break
                    }
                }
                
            } else {
                openIndexCountries.append(index)
            }
            
            self.view.reloadData()
            
        }
    }
    
    private func isOpenCountry(index:Int) -> Bool {
        for openIndex in openIndexCountries {
            if openIndex == index {
                return true
            }
        }
        
        return false
    }
    
}
