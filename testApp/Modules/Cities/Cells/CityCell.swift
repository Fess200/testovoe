//
//  CityCell.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol CityCellDelegate: NSObjectProtocol {
    func addFavourite(city:City)
    func removeFavourite(city:City)
}

class CityCell: UITableViewCell {

    @IBOutlet weak var buttonFavourite: UIButton!
    @IBOutlet weak var labelName: UILabel!
    
    weak var delegate:CityCellDelegate?
    
    var city: City! {
        didSet {
            labelName.text = city?.name
        }
    }
    
    var isFavourite: Bool = false {
        didSet {
            buttonFavourite.setTitle(isFavourite ? "Удалить" : "Добавить", for: .normal)
        }
    }
    
    @IBAction func onFavourite(_ sender: Any) {
        if isFavourite {
            delegate?.removeFavourite(city: city)
        } else {
            delegate?.addFavourite(city: city)
        }
    }

}
