//
//  CountryHeader.swift
//  testApp
//
//  Created by Andrew on 18.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol CountryHeaderDelegate: NSObjectProtocol {
    func tapHeaderCountry(country:Country)
}

class CountryHeader: UIView {

    @IBOutlet weak var labelName: UILabel!
    
    var country: Country! {
        didSet {
            labelName.text = country?.name
        }
    }
    
    weak var delegate:CountryHeaderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGester = UITapGestureRecognizer(target: self, action: #selector(tapHeader))
        self.addGestureRecognizer(tapGester)
    }
    
    @objc private func tapHeader() {
        delegate?.tapHeaderCountry(country: country)
    }
}
